# Strand
_Task-based multi-threading_

## Installing
Strand is published via JitPack, you can download it.
Alternatively, you can use Gradle or Maven for your dependency management.

### Gradle
Register the JitPack repository

	allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
	
Add Strand as a dependency

	dependencies {
	        implementation 'com.gitlab.CerebralFart:strand:Tag'
	}

### Maven
Register the JitPack repository

    <repositories>
		<repository>
		    <id>jitpack.io</id>
		    <url>https://jitpack.io</url>
		</repository>
	</repositories>
	
Add Strand as a dependency

    <dependency>
	    <groupId>com.gitlab.CerebralFart</groupId>
	    <artifactId>strand</artifactId>
	</dependency>
	
## Usage
Using Strand is quite simple, executing a task is as simple as submitting a lambda function.

    Strand strand = new Strand();
    strand.invokeLater(() -> {
        System.out.println("Executed asynchrounusly");
    });

But what if you want to do some long calculations in parallel? Strand allows you to do that as well.

    Task<Integer> task = strand.computeLater(() -> yourComplexFunction(1, 2));
    //Get a coffee
    int result = task.get();
    
## Questions, bugs or other issues?
Feel free to open an issue on [GitLab](https://gitlab.com/CerebralFart/strand/issues).