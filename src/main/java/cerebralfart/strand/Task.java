package cerebralfart.strand;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;

@Slf4j
public class Task<V> implements Future<V> {
	private static final long WAIT_TIMEOUT = 100L;

	private final Object sync = new Object();
	private final Strand strand;
	@Getter
	private boolean cancelled = false;
	private final Supplier<Boolean> check;
	private final Supplier<? extends V> callable;
	private boolean completed = false;
	private V result = null;

	Task(Strand strand, Supplier<Boolean> check, Supplier<? extends V> callable) {
		this.strand = strand;
		this.check = check;
		this.callable = callable;
	}

	boolean check() {
		return this.check.get();
	}

	void execute() {
		synchronized (this.sync) {
			this.result = this.callable.get();
			this.completed = true;
			this.sync.notifyAll();
		}
	}

	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		if (this.isDone()) {
			return false;
		} else {
			log.trace("Cancelling task {}", this.hashCode());
			this.strand.dequeue(this);
			this.cancelled = true;
			return true;
		}
	}

	@Override
	public boolean isDone() {
		synchronized (this.sync) {
			return this.completed;
		}
	}

	@Override
	public V get() throws InterruptedException {
		synchronized (this.sync) {
			log.trace("Getting result of task {}", this.hashCode());
			while (!this.isDone()) {
				this.sync.wait(WAIT_TIMEOUT);
			}
			return this.result;
		}
	}

	@Override
	public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		long throwAt = System.currentTimeMillis() + unit.toMillis(timeout);

		synchronized (this.sync) {
			log.trace("Getting result of task {}, timeout = {} {}", this.hashCode(), timeout, unit);
			while (!this.isDone()) {
				this.sync.wait(WAIT_TIMEOUT);
				if (System.currentTimeMillis() > throwAt)
					throw new TimeoutException("Getting result of task timed out");
			}

			return this.result;
		}
	}
}
