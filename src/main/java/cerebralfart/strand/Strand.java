package cerebralfart.strand;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

@Slf4j
public class Strand {
	private static final AtomicInteger idCounter = new AtomicInteger(1);
	private static final Supplier<Boolean> defaultCheck = () -> true;

	@Getter
	private final int id = idCounter.getAndIncrement();
	@Getter(AccessLevel.PACKAGE)
	private final Semaphore stopSemaphore = new Semaphore(0);
	private int targetThreadCount;
	private final Collection<Runner> runners = new LinkedList<>();
	private final Queue<Task<?>> queue = new LinkedList<>();

	public Strand() {
		this(Math.min(Runtime.getRuntime().availableProcessors(), 6));
	}

	public Strand(int threadCount) {
		this.startThreads(threadCount);
	}

	public void setThreadCount(int count) {
		if (count > this.targetThreadCount) {
			int diff = count - this.targetThreadCount;
			this.startThreads(diff);
		} else if (count < this.targetThreadCount) {
			int diff = this.targetThreadCount - count;
			this.stopThreads(diff);
		}
	}

	public void stop() {
		log.info("Stopping executor service");
		this.stopThreads(this.targetThreadCount);
	}

	private void startThreads(int count) {
		log.info("Starting {} runners", count);
		this.targetThreadCount += count;
		for (int i = 0; i < count; i++) {
			Runner runner = new Runner(this);
			this.runners.add(runner);
			runner.start();
		}
	}

	void notifyStop(Runner runner) {
		this.runners.remove(runner);
	}

	private void stopThreads(int count) {
		synchronized (this.queue) {
			log.info("Stopping {} runners", count);
			this.targetThreadCount -= count;
			this.stopSemaphore.release(count);
			this.queue.notifyAll();
		}
	}

	public void invokeLater(Runnable runnable) {
		this.invokeLater(defaultCheck, runnable);
	}

	public void invokeLater(Supplier<Boolean> check, Runnable runnable) {
		this.computeLater(check, () -> {
			runnable.run();
			return null;
		});
	}

	public <V> Future<V> computeLater(Supplier<? extends V> callable) {
		return this.computeLater(defaultCheck, callable);
	}

	public <V> Future<V> computeLater(Supplier<Boolean> check, Supplier<? extends V> callable) {
		Task<V> task = new Task<>(this, check, callable);
		this.queue(task);
		return task;
	}

	//Interval in milliseconds
	public void scheduleRecurring(Runnable runnable, double interval) {
		this.scheduleRecurring(runnable, () -> interval);
	}

	public void scheduleRecurring(Runnable runnable, Supplier<? extends Number> intervalSupplier) {
		long nextExecutionTime = System.currentTimeMillis() + intervalSupplier.get().longValue();
		this.invokeLater(
			() -> System.currentTimeMillis() > nextExecutionTime,
			() -> {
				runnable.run();
				this.scheduleRecurring(runnable, intervalSupplier);
			}
		);
	}

	//Explicitly done with notify (instead of notifyAll)
	//We don't need to wake up all threads for a single task
	void queue(Task<?> task) {
		synchronized (this.queue) {
			this.queue.add(task);
			this.queue.notify();
		}
	}

	Task<?> poll() {
		synchronized (this.queue) {
			return this.queue.poll();
		}
	}

	boolean hasTasks() {
		synchronized (this.queue) {
			return !this.queue.isEmpty();
		}
	}

	void queueWait(long time) {
		synchronized (this.queue) {
			try {
				this.queue.wait(time);
			} catch (InterruptedException ignored) {
			}
		}
	}

	void dequeue(Task<?> task) {
		synchronized (this.queue) {
			this.queue.remove(task);
		}
	}

	@Override
	public String toString() {
		return String.format("Strand-%d", this.id);
	}
}
