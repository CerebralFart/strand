package cerebralfart.strand;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class Runner {
	private static final long GET_TASK_SLEEP = 5000L;

	//TODO make this shared on Strand instance
	private static final AtomicInteger threadId = new AtomicInteger(1);

	private final int id = threadId.getAndIncrement();
	private final Strand strand;
	private final Thread thread;
	private boolean startable = true;
	private boolean running = false;

	Runner(Strand strand) {
		this.strand = strand;
		this.thread = new Thread(this::run);
		this.thread.setName(String.format(
			"Runner-%d-%d",
			this.strand.getId(),
			this.id
		));
	}

	void start() {
		if (this.startable) {
			this.startable = false;
			this.running = true;
			this.thread.start();
			log.debug("Starting thread {}", this.thread.getName());
		} else {
			log.warn("Attempted to restart thread {}", this.thread.getName());
		}
	}

	private void run() {
		while (this.running) {
			if (!this.strand.hasTasks()) {
				this.strand.queueWait(Runner.GET_TASK_SLEEP);
			}

			this.processTask();
			this.maybeStop();
		}
		log.debug("Stopped thread {}", this.thread.getName());
		this.strand.notifyStop(this);
	}

	private void processTask() {
		Task<?> task = this.strand.poll();
		if (task == null || task.isCancelled()) return;

		if (task.check()) {
			task.execute();
		} else {
			this.strand.queue(task);
		}
	}

	private void maybeStop() {
		if (this.strand.getStopSemaphore().tryAcquire()) {
			this.running = false;
			log.debug("Stopping thread {}", this.thread.getName());
		}
	}
}
